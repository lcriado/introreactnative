/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button, 
  ScrollView,
  TouchableOpacity
} from 'react-native';

import Icon from  'react-native-vector-icons/EvilIcons';
import moment from 'moment';
import Sprintf from 'sprintf-js';
import { BlurView } from 'react-native-blur';
import data from './src/data.js'

export default class App extends Component {
  render() {
    return (
      <ScrollView contentContainerStyles={styles.scrollContent}>
      {data.map((entry, idx) => {
          return (<CoffeBar key={idx} data = {data}/>)
        })}
      </ScrollView>
    );
  }
}

class CoffeBar extends Component {
  state = { index: 0, likes: 0 };

  onChangeButtonPress() {
    const currentState = this.state.index;
    if(currentState != 7) {
      this.setState({index: currentState + 1 });
      
    } else {
      this.setState({index: 0});
    }
  }

  onLikeButtonButtonPress() {
    const likes = this.state.likes + 1;
    this.setState({likes: likes });
    this.props.data[this.state.index].likes = likes;
  }

  render() {
    return (
      <BlurView style={styles.wrapper}>

        <Image 
          style={styles.image}
          source={{uri: this.props.data[this.state.index].image}} 
        />

        <Text style={styles.likes}> {this.props.data[this.state.index].likes} </Text>

        <BlurView style={styles.bottomWrapper} blurType='light' blurAmount={3}>
          <Text style={styles.location}>{this.props.data[this.state.index].location}</Text>
          <Text style={styles.author}>{this.props.data[this.state.index].author} on {moment(this.props.data[this.state.index].date).format("MM/DD/YYYY")} </Text>
          
          <TouchableOpacity style={styles.button} onPress={this.onLikeButtonButtonPress.bind(this)}>
            <Icon name="like" size={30} style={styles.icon} color='#FFFF' />
            <Text style={styles.buttonText}>LIKE!</Text>
          </TouchableOpacity>
      
        </BlurView>

        <Button
          onPress={this.onChangeButtonPress.bind(this)}
          title="Change Bar"
          color="#fff"
        />

      </BlurView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    top: 10,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  scrollContent: {
    paddingTop: 20
  },
  image: {
    width: '100%',
    height: 300
  },
  likes: {
    position: 'absolute',
    color: '#fff',
    fontSize: 20,
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 15,
    overflow: 'hidden',
    minWidth: 30,
    height: 30,
    top: 10,
    right: 10,
    backgroundColor: '#0009',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center'
},
  location: {
    fontSize: 18,
    color: '#333333'
  },
  author: {
    fontSize: 16,
    color: '#333333'
  },
  button: {
    position: 'absolute',
    right: 5,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    overflow:'hidden',
    backgroundColor: '#3b5998',
    padding: 5,
    borderRadius: 5
  },
  buttonText: {
    color:'#FFFF',
    fontSize: 16,
    fontWeight: 'bold'
  },
  bottomWrapper: {
    justifyContent: 'center',
    padding: 5
  }
});