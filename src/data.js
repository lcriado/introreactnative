export default data = 
[{
  image: 'https://images.unsplash.com/5/unsplash-bonus.jpg?auto=compress,format&fit=crop&w=640&h=480&q=60',
  location: 'Kitsuné Café, Montreal, Canada',
  author: 'Luke Chesser',
  likes: 0,
  date: 1429681628348
},
{
  image: 'https://i.pinimg.com/originals/d6/ef/bb/d6efbb0bc3f5edf57f38504b57bd0cdb.png',
  location: 'Somewhere in Barcelona',
  author: 'Lluis Criado',
  likes: 0,
  date: 1429192701148

}, 
{
    image: 'https://images.unsplash.com/photo-1429681601148-75510b2cef43?auto=compress,format&fit=crop&w=640&h=480&q=60',
    location: 'Café Kahvipuu, Kokkola',
    author: 'Seemi Samuel',
    likes: 0,
    date: 1429681601148
},
{
    image: 'https://images.unsplash.com/photo-1453614512568-c4024d13c247?dpr=1&auto=compress,format&fit=crop&w=376&h=250&q=60',
    location: 'Bintaro, Indonesia',
    author: 'Nafinia Putra',
    likes: 0,
    date:1453614512568
},
{
    image: 'https://images.unsplash.com/photo-1489396944453-834c536105c0?dpr=1&auto=compress,format&fit=crop&w=376&h=251&q=60',
    location: 'Bodø, Bodo, Norway',
    author: 'Thomas Litangen',
    likes: 0,
    date:1489396944453
},
{
    image: 'https://images.unsplash.com/5/unsplash-bonus.jpg?auto=compress,format&fit=crop&w=640&h=480&q=60',
    location: 'Kitsuné Café, Montreal',
    author: 'Luke Chesser',
    likes: 0,
    date:1452771273041
},
{
    image: 'https://images.unsplash.com/photo-1452776145041-517a74be1f14?dpr=1&auto=compress,format&fit=crop&w=376&h=250&q=60',
    location: '26 Rathbone Pl, London',
    author: 'Jordan Sanchez',
    likes: 0,
    date:1452776145041
},
{
    image: 'https://images.unsplash.com/photo-1441336558687-a06957a7642a?dpr=1&auto=compress,format&fit=crop&w=376&h=250&q=60',
    location: 'Iconoclast Koffiehuis, Edmonton',
    author: 'Redd Angelo',
    likes: 0,
    date:1441336558687
}];